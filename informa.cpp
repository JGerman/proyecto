#ifndef _INFORMA_IMPL_H_V1
#define _INFORMA_IMPL_H_V1

/* 
	Para ver el texto adecuadamente: 
		Tipografía del editor: Monospace Regular 12
		Anchura del tabulador: 4
	
	Ubicaciones de apoyo *******************************************************
		https://es.cppreference.com/w/cpp/preprocessor/conditional
		https://www.cprogramming.com/reference/preprocessor/ifndef.html
*/

#include "informa.h"

/* Implementaciones de Informa ************************************************/
/** Asignador, también inicializador */
void
Informa::init(bool b, std::string m, int ce){
	this -> hubo_error = b;
	
	if(hubo_error){
		this -> detalles_del_error 	= m;
		this -> codigo_del_error 	= ce;
	}
	
	//	std::cout<<" Hubo_error: "<< hubo_error<<std::endl;
	//	std::cout<<" Detalles del mensaje: "<< detalles_del_error<<std::endl;
	//	std::cout<<" Código de error: "<< codigo_del_error<<std::endl;
}

/** Constructores *************************************************************/
Informa::Informa(){ init(true, "", 0);}
Informa::Informa(bool e, std::string m, int ce){ init(e, m, ce);}


/** Consultores ***************************************************************/
bool
Informa::get_error(){		return hubo_error;}

std::string 
Informa::get_message(){		return detalles_del_error;}

int
Informa::get_cerror(){		return codigo_del_error;}


/** Modificadores *************************************************************/
void
Informa::set(bool b, std::string m, int ce){ init(b, m, ce);}

void
Informa::set(){
	if(errno != 0){ init(true, strerror(errno), errno);}
}

/** Otras funciones ***********************************************************/
std::string
Informa::to_string(){
	std::string estado_actual ="";
	estado_actual += (hubo_error?" true":" false"); 
	estado_actual += "\n ";
	estado_actual += this-> detalles_del_error;
	estado_actual += "\n ";
	estado_actual += std::to_string(codigo_del_error);
	
	return estado_actual;
}


#endif

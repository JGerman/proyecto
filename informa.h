#ifndef _CABECERAS_DE_INFORMA_H_
#define _CABECERAS_DE_INFORMA_H_

/* 
	Para ver el texto adecuadamente: 
		Tipografía del editor: Monospace Regular 12
		Anchura del tabulador: 4
	
	Ubicaciones de apoyo *******************************************************
		https://es.cppreference.com/w/cpp/preprocessor/conditional
		https://www.cprogramming.com/reference/preprocessor/ifndef.html
*/
#include <string>
#include <errno.h>
#include <string.h> // Exclusivamente para poder usar strerror( __numerror )

class Informa{
	private:
	// Atributos
	bool hubo_error;
	std::string detalles_del_error;
	int codigo_del_error;
	
	// Asignador o Inicializador
	void init(bool b, std::string m, int ce);
	
	public:
	// Constructores
	Informa();
	Informa(bool b, std::string m, int codigo);
	
	// Consultores 'get'
	bool get_error();
	std::string get_message();
	int get_cerror();
	
	// Modificadores 'set'
	void set(bool b, std::string m, int ce);	// Personalizable
	void set();									// Aportado por el Sistema
	
	// Otras funciones
	std::string to_string();
};


#endif

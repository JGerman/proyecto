#ifndef _CABECERAS_DE_LISTADO_IMPL_H_
#define _CABECERAS_DE_LISTADO_IMPL_H_

/* 
	Para ver el texto adecuadamente: 
		Tipografía del editor: Monospace Regular 12
		Anchura del tabulador: 4
	
	Ubicaciones de apoyo *******************************************************
		https://es.cppreference.com/w/cpp/preprocessor/conditional
		https://www.cprogramming.com/reference/preprocessor/ifndef.html
*/

#include "listado.h"
#include <dirent.h>


Listado::Listado(int argc, char* argv[]){
	// comprueba el número de parámetros y el parámetro 1
	comprobar(argc, argv);
	
	if(!informe.get_error()){ 
	// listar_archivos comprueba el parámetro 2 
		listar_archivos(argv[2], patron);	
	}
	
	if(!informe.get_error()){
	// Asignia el directorio donde se encuentran los elementos
		this -> ruta_del_directorio = argv[2];	
	
	// Inicializa las variables relacionadas con el vector elementos
	//	init();
	}
	
	init();	
}

/* [1] Instacia las variables estrechamente vinculadas al vector de elementos */
// PRE: El vector elementos debe ser no nulo.
void
Listado::init(){
	for(int i=0; i<elementos.size(); i++){
		this -> elementos_activos.push_back(true);
		this -> accesos.push_back(0);
	}
	
	this -> accesos_totales = 0;
}


/* [2] ************************************************************************/
// PRE: this -> informe, está definido
Informa
Listado::comprobar(int argc, char* argv[]){
	// Comprobando cantidad de argumentos 
	if(argc == 3){
	
	// Comprobando el patrón del primer argumento
		if(argv[1][0] == '-'){
		this -> informe.set(false, "Primer argumento bien especificado", 0);
		
	// Obtiene el texto a buscar
			std::string texto_a_buscar = argv[1];
			int longitud = texto_a_buscar.size();
			this -> patron = texto_a_buscar.substr(1, longitud);
		}
	// Muestra el error: Segundo argumento mal escrito
		else{
			std::string mensaje = "Primer argumento mal escrito";
			mensaje = mensaje + " (" + argv[1] + ")";
			
			mensaje += "\n" + help(argc, argv);
			this -> informe.set(true, mensaje, 1);
		}
	}
	// Muestra error: Cantidad de argumentos distinta de tres
	else{
		std::string mensaje = "Número de argumentos incorrecto";
		mensaje.append(" (");
		mensaje.append(std::to_string(argc));
		mensaje.append(")");
		
		mensaje.append("\n");
		mensaje.append(help(argc, argv));
		this -> informe.set(true, mensaje, 2);
	}
	
	return informe;
}

/* [3] Recorre directorio *****************************************************/
Informa
Listado::listar_archivos(std::string dir, std::string texto){
	
	DIR* directorio;
	struct dirent* elemento;
	std::string item;
	
	// Verificando que es un directorio (Si la asignación es correcta) 
	if(directorio = opendir(dir.c_str())){
	
	
	// Recorriendo el directorio
		while( elemento = readdir(directorio)){
			item = elemento -> d_name;
				
	// Incluyendo exclusivamente los nombres de los archivos ply	
			if(item.rfind(texto)< SIZE_MAX){			
				this -> elementos.push_back(item);
			}
		}
	}
	else{
	// Hubo algún error al abrir el directorio
		this -> informe.set();
	}
	
	closedir(directorio);
	return informe;
}

/* []  dentro: Comprueba que i se encuentra dentro de los límites *************/
bool
Listado::dentro_del_rango(int i){
	bool dentro = (0<=i && i< elementos.size());

	this -> informe.set(!dentro, " Fuera de Rango "+ std::to_string(i), 3);
	return dentro;
}

/* [] *************************************************************************/
void
Listado::commute(int i){
	if(dentro_del_rango(i))
		this -> elementos_activos[i]= !elementos_activos[i];
}

/* [] *************************************************************************/
std::string
Listado::get(int i){
	std::string resultado = "·";
	if(dentro_del_rango(i)){
		resultado = elementos[i];
		accesos[i]++;
		accesos_totales ++;
	}
	
	return resultado;
}

/* [] */
std::string
Listado::get_r(int i){
	std::string item = get(i);
	
	if(informe.get_error()){
		item = ruta_del_directorio + "/" + item ;
	}
		
	return item;
}

/* [] */
std::string
Listado::get_active(int i){
	int k = 0;
	if(dentro_del_rango(i)){
		for(int j=0; k<i && j<elementos.size(); j++){
			k += elementos_activos[j];
		}
	}
	
	return get(k);
}

/* [] */
std::string
Listado::get_active_r(int i){
	std::string item = get_active(i);
	if(!informe.get_error()){
		item = ruta_del_directorio + "/" + item;
	}
	return item;
}


/* [] *************************************************************************/
std::string 
Listado::to_string(){
	std::string estado_actual = "";
	
	estado_actual += informe.get_message();
	if(!informe.get_error()){
		estado_actual += "  Ruta del directorio: " + this -> ruta_del_directorio;
		estado_actual += "\n  Elementos con patrón: " + this -> patron;
		estado_actual += "\n  Accesos totales: " + std::to_string(accesos_totales);
		estado_actual += "\n  Elementos (";
		estado_actual += std::to_string(elementos.size());
		estado_actual += ")\n";
	//estado_actual += informe.to_string();
	
	
		std::string activo;
		for(int i=0; i< elementos.size(); i++){
			activo = (elementos_activos[i]?"A":"·");
			estado_actual += "\t[" + std::to_string(i) + "] "
					  + "["+ activo + "] " 
					  + elementos[i] + "\n";
		}
	}
	
	return estado_actual;
}

/* [] *************************************************************************/
std::string
Listado::actives(bool b){
	std::string items;
	
	int j=0;
	for(int i=0; i< elementos.size(); i++){
		if(elementos_activos[i] == b){
			items += "\t[" + std::to_string(j) + "] " 
				+ elementos[i] + "\n";
				j++;
		}
	}
	
	return items;
}

/* [] *************************************************************************/
std::string
Listado::help(int argc, char* argv[]){
	std::string modo_de_uso=" ";
	
	if(this -> informe.get_error()){
		modo_de_uso = " Usage:     ";
		modo_de_uso.append(argv[0]);
		modo_de_uso += " -texto_a_buscar directorio ";
	}
	
	modo_de_uso +="\n You wrote: ";
	for(int i=0; i<argc; i++){
		modo_de_uso.append(argv[i]);
		modo_de_uso += " ";
	}
	
	return modo_de_uso;
}


/* [] *************************************************************************/
std::string
Listado::status(){
	std::string lista;
	
	if(informe.get_error()){
		lista = informe.get_message();
	}
	else{
		lista = " Elementos:";
		std::string activo;
		for(int i=0; i< elementos.size(); i++){
			activo = (elementos_activos[i]?"A":"·");
			lista += "\n\t[" + std::to_string(i) + "] "
					  + "["+ activo + "] " 
					  + elementos[i];
		}
	}
	
	return lista;
}

/* [] *************************************************************************/
Informa
Listado::get_informe(){ return this-> informe;}


/* [] *************************************************************************/
int
Listado::size(){ return this -> elementos.size();}

/* [] *************************************************************************/
bool
Listado::active(int i){ 
	bool existe = dentro_del_rango(i);
	bool item_activo;
	
	if(existe){
		item_activo = this -> elementos_activos[i];
	}
	else{
		item_activo = false;
	}
	
	return item_activo;
}












#endif

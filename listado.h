#ifndef _CABECERAS_DE_LISTADO_H_
#define _CABECERAS_DE_LISTADO_H_

/* 
	Para ver el texto adecuadamente: 
		Tipografía del editor: Monospace Regular 12
		Anchura del tabulador: 4
	
	Ubicaciones de apoyo *******************************************************
		https://es.cppreference.com/w/cpp/preprocessor/conditional
		https://www.cprogramming.com/reference/preprocessor/ifndef.html
*/
#include <string>
#include <vector>
#include "informa.h"

class Listado{
	
	// Atributos
	private:
	std::vector<std::string> elementos;
	std::vector<bool> elementos_activos;
	std::string ruta_del_directorio;
	std::string patron;
	
	std::vector<int> accesos;
	int accesos_totales;
	
	Informa informe;
	
	// Métodos privados
	void init();
	Informa comprobar(int argc, char* argv[]);
	Informa listar_archivos(std::string dir, std::string patron);
	bool dentro_del_rango(int i);
	
			
	// Métodos públicos	
	public:
	Listado(int argc, char* argv[]);
	std::string help(int argc, char* argv[]);
	std::string get(int i);
	std::string get_r(int i);
	std::string get_active(int i);
	std::string get_active_r(int i);
	void commute(int i);
	Informa get_informe();
	int size();
	bool get_active(int i);
	
	std::string actives(bool b= true);
	std::string to_string();
	std::string status();

	
	

};

#endif

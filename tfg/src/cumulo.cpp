#ifndef _CABECERAS_DE_CUMULO_IMPL_H_
#define _CABECERAS_DE_CUMULO_IMPL_H_

#include <thread>

//
#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/visualization/keyboard_event.h>  // Capturar eventos
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/ply_io.h>
#include <pcl/console/parse.h>

#include "cumulo.h"

// [1] Instancia las variables estrechamente vinculadas al vector de elementos "El vector de nubes asociadas"
void
Cumulo::init(){

// Cargando nubes primigenias y añadiendolas al vector de nubes
	for(int i=0; i<lista_primigenia.size(); i++){
		pcl::PointCloud<PointT>::Ptr nube(new pcl::PointCloud<PointT>);

		std::cout<< "ruta: "<< lista_primigenia.get_r(i)<< std::endl;
		pcl::io::loadPLYFile(lista_primigenia.get_r(i), *nube);
		vector_primigenio_ptr.push_back(nube);
	}

// Incluyendo patron y ruta de la lista_primigenia a la lista_emergente
	lista_emergente.set_path(	lista_primigenia.get_path());
	lista_emergente.add_pattern(lista_primigenia.get_pattern());


/*	this -> visor	= new pcl::visualization::PCLVisualizer("Nubes con patrón [X] ");

	this -> esc		= false;
	space();
*/
};


Cumulo::Cumulo(){};
Cumulo::Cumulo(const Cumulo &c){
	this -> lista_primigenia	= c.lista_primigenia;
	this -> lista_emergente		= c.lista_emergente;
//	this -> visor				= c.visor;
//	this -> esc					= c.esc;
};

Cumulo::Cumulo(int argc, char* argv[]){
	lista_primigenia = Listado(argc, argv);
	init();
};

/*
Cumulo::~Cumulo(){
	int tamanio = vector_primigenio_ptr.size();

	for(int i=0; i< tamanio; i++){
		vector_primigenio_ptr.pop_back();
	}
}
*/

void
Cumulo::addClouds(pcl::visualization::PCLVisualizer::Ptr v, bool origen){
	if(origen){
		for(int i=0; i<lista_primigenia.size(); i++){
			v -> addPointCloud<PointT>(vector_primigenio_ptr[i], lista_primigenia.get(i));
		}
	}
	else{
		for(int i=0; i<lista_emergente.size(); i++){
			v -> addPointCloud<PointT>(vector_emergente_ptr[i], lista_emergente.get(i));
		}
	}


}

std::string
Cumulo::status(){
/*
	std::string estado = "";

	for(int i=0; i< lista_primigenia.size(); i++){
		estado 	+= "\n\t["+ std::to_string(i) +"] "
				+ lista_primigenia.get(i);
	}

	return estado;
*/
	return lista_primigenia.status();
};

#include <sys/stat.h>
void
Cumulo::save(std::string nombre_directorio){

	std::string nueva_ruta = lista_emergente.get_path()
		+ "/"+nombre_directorio;

	if(mkdir(nueva_ruta.c_str(), 0777) == 0){
		std::cout<<" Carpeta creada "<< std::endl;
	}
	else{
		std::cout<<" Carpeta NO creada "<< std::endl;
	}

	lista_emergente.set_path(nueva_ruta);

	for(int i=0; i< lista_emergente.size(); i++){
		pcl::io::savePLYFile(
			nueva_ruta+"/"+lista_emergente.get(i),
			*vector_emergente_ptr.at(i)
		);
	}

}

/*
// Eventos
// Eventos de teclado
void
keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event){
	if (event.keyDown()){
		std::cout<< "Tecla presionada "<< event.getKeySym()<< std::endl;
	}
	// this -> esc = event.getKeySym() == "Escape";
};

// Eventos de ratón
void
mouseEventOccurred(const pcl::visualization::MouseEvent &event){
	if (event.getButton() == pcl::visualization::MouseEvent::LeftButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón izquierdo "<< std::endl;
	}
	else if (event.getButton() == pcl::visualization::MouseEvent::RightButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón derecho "<< std::endl;
	}

};


// Espacio

void
Cumulo::space (double r, double g, double b){

//	Añadiendo color de fondo
	visor.setBackgroundColor(r, g, b);

//	Añadiendo Ejes de coordenadas
	visor.addCoordinateSystem(1.0);

//	Añadiendo texto-guía a los ejes de coordenadas
//	pcl::PointXYZ punto(X, Y, Z)
	PointT EjeX(1, 0, 0);
	PointT EjeY(0, 1, 0);
	PointT EjeZ(0, 0.01, 1);
	PointT O(-0.1,0, -0.1);

//	addText3D(texto, punto_de_anclaje, escalado_del_texto, rojo, verde, azul)
	visor.addText3D("+X", EjeX, 0.1, 1, 0, 0);
	visor.addText3D("+Y", EjeY, 0.1, 0, 1, 0);
	visor.addText3D("+Z", EjeZ, 0.1, 0, 0, 1);
	visor.addText3D("O", O, 0.1, 1,1,1);

//	Orientando el foco de visión (la cámara)
//	visor.initCameraParameters();

	std::cout<<" Espacio creado. "<<std::endl;

//	Registra los clicks de raton y las pulsaciones del teclado
    visor.registerKeyboardCallback (keyboardEventOccurred);
	visor.registerMouseCallback (mouseEventOccurred);
};
*/

std::string
Cumulo::coordenadaX(double centro, double tolerancia){
	std::string valores_x="";
	double valor_x;
	double extremo_inf = centro-tolerancia;
	double extremo_sup = centro+tolerancia;

	pcl::PointCloud<PointT>::Ptr nube;
	pcl::PointCloud<PointT>::Ptr nueva_nube(new pcl::PointCloud<PointT>);

	for(int i_nube=0; i_nube < lista_primigenia.size(); i_nube++){
		nube = vector_primigenio_ptr[i_nube];
		valores_x 	+= " "+ lista_primigenia.get(i_nube)
					+ " ("+ std::to_string(nube-> size()) + "):\n";

		for(int i_punto=0; i_punto < nube -> size(); i_punto++){
			valor_x = nube -> points[i_punto].x;
			if( extremo_inf < valor_x && valor_x < extremo_sup ){
				valores_x += "\t"+ std::to_string(valor_x)+ "\n";
				nueva_nube -> push_back( nube-> points[i_punto]);
			}
		}
	}

// FIXMI: Esta tarea debe extraerse para poder ser llamada desde otros métodos que generen nuevas nubes.
	vector_emergente_ptr.push_back(nueva_nube);
	lista_emergente.add(
	"centro_"	+ std::to_string(centro)
	+ "_tol_"		+ std::to_string(tolerancia)
	+ "." + lista_emergente.get_pattern());

	return valores_x;
}

#endif

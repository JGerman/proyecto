#ifndef _CABECERAS_DE_CUMULO_H_
#define _CABECERAS_DE_CUMULO_H_

#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/keyboard_event.h>

#include "listado.h"
#include <string>
#include <vector>

// Útil para cambiar rápidamente el tipo de punto
typedef pcl::PointXYZ PointT;


class Cumulo{
	private:
// Atributos
	Listado lista_primigenia, lista_emergente;
//	pcl::visualization::PCLVisualizer visor;
	std::vector<pcl::PointCloud<PointT>::Ptr> vector_primigenio_ptr;
	std::vector<pcl::PointCloud<PointT>::Ptr> vector_emergente_ptr;

	bool esc;

// Asignador o inicializador
	void init();
//	void space (double r=0.9, double g=0.8, double b=0.7);
//	void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event);
//	void mouseEventOccurred(const pcl::visualization::MouseEvent &event);


	public:
// Constructores
	Cumulo();
	Cumulo(const Cumulo &c);
	Cumulo(int argc, char* argv[]);
//	~Cumulo();

//
	void addClouds(pcl::visualization::PCLVisualizer::Ptr v, bool origen);
	void save(std::string nombre_directorio);

	std::string status();

//	void X(int valor);
	std::string coordenadaX(double centro, double tolerancia);

};

#endif



#include <iostream>
#include <thread>


#include "listado.h"
#include "cumulo.h"

// Útil para cambiar rápidamente el tipo de punto
typedef pcl::PointXYZ PointT;

// Necesario para poder incluir ---->  std::this_thread::sleep_for(100ms);
 using namespace std::chrono_literals;

pcl::visualization::PCLVisualizer::Ptr
space (pcl::visualization::PCLVisualizer::Ptr visor,
			double r=0.9, double g=0.8, double b=0.7);

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event);

void mouseEventOccurred(const pcl::visualization::MouseEvent &event);

bool esc=false;


/* MAIN */
int main(int argc, char* argv[]){

/*
	Cumulo cumulo_con_parametros(argc, argv);
	std::cout<< " Creado cúmulo con parametros"<< std::endl;
	std::cout<< cumulo_con_parametros.status()<< std::endl;
	
	std::cout<< "\n Mostrando resultado del método coordenadaX()"<< std::endl;
	std::cout<< cumulo_con_parametros.coordenadaX(2.5, 0.3)<< std::endl;
	
	Cumulo cumulo_sin_parametros;
	std::cout<< "\n Creado cúmulo sin parametros"<< std::endl;
	std::cout<< cumulo_sin_parametros.status()<< std::endl;

	cumulo_sin_parametros = Cumulo(argc, argv);
	std::cout<< "\n Asignado al cúmulo sin parametros un cumulo por constructor de copia "<< std::endl;
	std::cout<< cumulo_sin_parametros.status()<< std::endl;
*/


/**/
	Cumulo c(argc, argv);
	std::cout<< "Contenido del cumulo "<< std::endl;
	std::cout<< c.status()<< std::endl;

	pcl::visualization::PCLVisualizer::Ptr visor(new pcl::visualization::PCLVisualizer("Control de nubes"));
	space(visor);

	// Método que selecciona los puntos comprendidos entre [2.5-1.3 , 2.5+1.3 ]
	// Una rodaja de ancho 2.6 centrada en 2.5
	std::string valores = c.coordenadaX(2.5, 1.3);

	// Con true, muestra las nubes primigenias
	// con false, muestra las nubes emergentes
    c.addClouds(visor, false);

	while(!esc){
		visor -> spinOnce (100);
		std::this_thread::sleep_for(100ms);
	}

	std::cout<< "\n Valores encontrados: \n"<< valores<< std::endl;
	c.save("nubes_emergentes");
/**/

}
/**********************************************************************/

/* Implementacion de las funciones */
pcl::visualization::PCLVisualizer::Ptr
space (pcl::visualization::PCLVisualizer::Ptr visor,
	double r, double g, double b){

//	Añadiendo color de fondo
	visor -> setBackgroundColor(r, g, b);
	
//	Añadiendo Ejes de coordenadas
	visor -> addCoordinateSystem(1.0);
	
//	Añadiendo texto-guía a los ejes de coordenadas
//	pcl::PointXYZ punto(X, Y, Z)
	PointT EjeX(1, 0, 0);
	PointT EjeY(0, 1, 0);
	PointT EjeZ(0, 0.01, 1);
	PointT O(-0.1,0, -0.1);
	
//	addText3D(texto, punto_de_anclaje, escalado_del_texto, rojo, verde, azul)
	visor -> addText3D("+X", EjeX, 0.1, 1, 0, 0);
	visor -> addText3D("+Y", EjeY, 0.1, 0, 1, 0);
	visor -> addText3D("+Z", EjeZ, 0.1, 0, 0, 1);
	visor -> addText3D("O", O, 0.1, 1,1,1);
	
//	Orientando el foco de visión (la cámara)
//	visor.initCameraParameters();
	
	std::cout<<" Espacio creado. "<<std::endl;


//	Registra los clicks de raton y las pulsaciones del teclado
    visor -> registerKeyboardCallback (keyboardEventOccurred);
	visor -> registerMouseCallback (mouseEventOccurred);

	return visor;
}

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event){
	if (event.keyDown()){
		std::cout<< "Tecla presionada "<< event.getKeySym()<< std::endl;
	}
	esc = event.getKeySym() == "Escape";
}


void mouseEventOccurred(const pcl::visualization::MouseEvent &event){
	if (event.getButton() == pcl::visualization::MouseEvent::LeftButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón izquierdo "<< std::endl;
	}
	else if (event.getButton() == pcl::visualization::MouseEvent::RightButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón derecho "<< std::endl;
	}

}





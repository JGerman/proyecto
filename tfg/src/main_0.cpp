/* Código basado en:
  //http://www.pointclouds.org/documentation/tutorials/pcl_visualizer.php#pcl-visualizer
 */
#include <iostream>
#include <thread>

//
#include <pcl/visualization/pcl_visualizer.h>
//#include <pcl/visualization/keyboard_event.h>  // Capturar eventos
#include <pcl/point_types.h>
#include <pcl/common/common_headers.h>
#include <pcl/features/normal_3d.h>
#include <pcl/io/ply_io.h>
#include <pcl/console/parse.h>

// 
#include <vector>
#include <string>
#include "listado.h"
#include "menu.h"

// Útil para cambiar rápidamente el tipo de punto
typedef pcl::PointXYZ PointT;

// Necesario para poder incluir ---->  std::this_thread::sleep_for(100ms);
 using namespace std::chrono_literals;

/* Variables globales y Cabeceras de funciones */
void space (pcl::visualization::PCLVisualizer& visor,
			double r=0.9, double g=0.8, double b=0.7);

pcl::visualization::PCLVisualizer::Ptr
space (pcl::visualization::PCLVisualizer::Ptr visor,
			double r=0.9, double g=0.8, double b=0.7);

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event);

void mouseEventOccurred(const pcl::visualization::MouseEvent &event);

bool esc=false;


/* MAIN */
int main(int argc, char* argv[]){
	
//	Creando la nube auxiliar
	pcl::PointCloud<PointT> nube;
	
//	Creando vector de punteros inteligentes
	std::vector<pcl::PointCloud<PointT>::Ptr> vector_nube_ptr;

//	Obteniendo las lista de nubes a mostrar
	Listado lista(argc, argv);

//	Creando menú
	Menu menu;


/* Comprobando estado de la construcción de 'lista'
 * Si se introducen directamente los archivos ply en la terminal
 * no tengo control sobre estos en esta versión.
 */

	if(lista.get_error()){
		std::cout<<lista.get_informe()<<std::endl;

//	FIXMI: ¿Debo dar la opción de añadir manualmente los PLY?
/*
		for(int i=0; i< argc; i++){
			pcl::io::loadPLYFile(argv[i], *nube_ptr);
			vector_nube_ptr.push_back(nube_ptr);
		
//	Añadiendo las nubes al observador /ç
			viewer.addPointCloud<PointT>(nube.makeShared(), argv[i]);
		}
*/
	}
	else{
//	Actualizando menu.
		menu.set(lista);
		for(int i=0; i<lista.size(); i++){

			pcl::io::loadPLYFile(lista.get_active_r(i), nube);
			vector_nube_ptr.push_back(nube.makeShared());

//	Añadiendo las nubes al observador
//			viewer.addPointCloud<PointT>(nube.makeShared(), lista.get_active(i));
		}
	}

//	Creando al observador y añadiendole coordenadas y funciones de captacion de eventos
	pcl::visualization::PCLVisualizer::Ptr visor(new pcl::visualization::PCLVisualizer("Control de nubes"));
	space(visor);
	
	
	do{
		while(!menu.exit()){
//	FIXMI: No entiendo porque la lista pasada por referencia
//	en menu, al alterar la interna no altera esta externa.
			lista = menu.show();
		}

//	Aqui toca incluir nubes si no existen y borrar nubes declaradas inactivas.
		for(int i=0; i<lista.size(); i++){
			if(lista.active(i)){
				std::cout<< " "<< lista.get(i)<< "\t activo "
						 << ", estado: "<< lista.active(i)<<std::endl;

				if(!visor -> contains(lista.get(i))){
				visor -> addPointCloud<PointT>(vector_nube_ptr[i], lista.get(i));
				}
			}
			else{
				visor -> removePointCloud(lista.get(i));
			}
		}

		while(!esc){
			visor -> spinOnce (100);
			std::this_thread::sleep_for(100ms);
		}

//	Reseteando las opciones
		menu.reset_option();
		esc = false;

    }while(!visor-> wasStopped());

	return 0;
}

/* Implementacion de las funciones */
void space (pcl::visualization::PCLVisualizer& visor,
 			double r, double g, double b){
	
//	Añadiendo color de fondo
	visor.setBackgroundColor(r, g, b);
	
//	Añadiendo Ejes de coordenadas
	visor.addCoordinateSystem(1.0);
	
//	Añadiendo texto-guía a los ejes de coordenadas
//	pcl::PointXYZ punto(X, Y, Z)
	PointT EjeX(1, 0, 0);
	PointT EjeY(0, 1, 0);
	PointT EjeZ(0, 0.01, 1);
	PointT O(-0.1,0, -0.1);
	
//	addText3D(texto, punto_de_anclaje, escalado_del_texto, rojo, verde, azul)
	visor.addText3D("+X", EjeX, 0.1, 1, 0, 0);
	visor.addText3D("+Y", EjeY, 0.1, 0, 1, 0);
	visor.addText3D("+Z", EjeZ, 0.1, 0, 0, 1);
	visor.addText3D("O", O, 0.1, 1,1,1);
	
//	Orientando el foco de visión (la cámara)
//	visor.initCameraParameters();

	std::cout<<" Espacio creado. "<<std::endl;
}


/* Implementacion de las funciones */
pcl::visualization::PCLVisualizer::Ptr
space (pcl::visualization::PCLVisualizer::Ptr visor,
	double r, double g, double b){

//	Añadiendo color de fondo
	visor -> setBackgroundColor(r, g, b);
	
//	Añadiendo Ejes de coordenadas
	visor -> addCoordinateSystem(1.0);
	
//	Añadiendo texto-guía a los ejes de coordenadas
//	pcl::PointXYZ punto(X, Y, Z)
	PointT EjeX(1, 0, 0);
	PointT EjeY(0, 1, 0);
	PointT EjeZ(0, 0.01, 1);
	PointT O(-0.1,0, -0.1);
	
//	addText3D(texto, punto_de_anclaje, escalado_del_texto, rojo, verde, azul)
	visor -> addText3D("+X", EjeX, 0.1, 1, 0, 0);
	visor -> addText3D("+Y", EjeY, 0.1, 0, 1, 0);
	visor -> addText3D("+Z", EjeZ, 0.1, 0, 0, 1);
	visor -> addText3D("O", O, 0.1, 1,1,1);
	
//	Orientando el foco de visión (la cámara)
//	visor.initCameraParameters();
	
	std::cout<<" Espacio creado. "<<std::endl;


//	Registra los clicks de raton y las pulsaciones del teclado
    visor -> registerKeyboardCallback (keyboardEventOccurred);
	visor -> registerMouseCallback (mouseEventOccurred);

	return visor;
}

void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event){
	if (event.keyDown()){
		std::cout<< "Tecla presionada "<< event.getKeySym()<< std::endl;
	}
	esc = event.getKeySym() == "Escape";
}


void mouseEventOccurred(const pcl::visualization::MouseEvent &event){
	if (event.getButton() == pcl::visualization::MouseEvent::LeftButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón izquierdo "<< std::endl;
	}
	else if (event.getButton() == pcl::visualization::MouseEvent::RightButton && event.getType() == pcl::visualization::MouseEvent::MouseButtonRelease){
		std::cout<< " ("<< event.getX()<<", "<<event.getY()<<") botón derecho "<< std::endl;
	}

}

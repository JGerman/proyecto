


#include <iostream>
#include "menu.h"


Menu::Menu(){
	init();
}

Menu::Menu(Listado & l){
	set(l);
	init();
}

void
Menu::init(){
	std::cout<<" Instanciando vector de opciones "<< std::endl;
	
	opciones.push_back(" Mostrar todos los elementos.");
	opciones.push_back(" Mostrar los elementos activo.");
	opciones.push_back(" Mostrar los elementos inactivos.");
	opciones.push_back(" Activar/Desactivar elemento. ");
	opciones.push_back(" Ver ruta de un elemento. ");
	opciones.push_back(" Borrar todo.");
	opciones.push_back(" Salir.");
	
	opcion_elegida = -1;
	condicion_de_salida = opciones.size()-1;
}

void
Menu::set(Listado& l){
	this -> lista = l;
}

Listado
Menu::show(){
	std::cout<< "\n Opciones disponibles: "<< std::endl;
	for(int i=0; i<opciones.size(); i++){
		std::cout<< " ["<< i<< "] "<< opciones[i]<<std::endl;
	}
	
	std::cout<< " opción: ";
	std::cin>>opcion_elegida;
	std::cout<<std::endl;
	

	realizar_opcion();

	return lista;
}

int
Menu::get_option(){ return this -> opcion_elegida;}

void
Menu::reset_option(){ opcion_elegida = 0;}

bool
Menu::exit(){
	return opcion_elegida == condicion_de_salida;
}

void
Menu::realizar_opcion(){
	std::string resolucion;
	
	switch(opcion_elegida){
		case 0:		// Mostrar todos los elementos
			resolucion = lista.status();
			break;
			
		case 1:		// Mostrar los elementos activos
			resolucion = lista.actives(true);
			break;
			
		case 2:		// Mostrar los elementos inactivos
			resolucion = lista.actives(false);
			break;
			
		case 3:		// Activar/Desactivar elemento
			std::cout<<lista.status()<<std::endl;
			int item_a_conmutar;
			std::cout<<" ¿Cual quiere conmutar? ";
			std::cin>>item_a_conmutar;
			std::cout<<" item elegido: "<< item_a_conmutar<< std::endl;
			lista.commute(item_a_conmutar);
			break;
			
		case 4:
			std::cout<<lista.status()<<std::endl;
			int item_a_mostrar;
			std::cout<<" ¿Que elemento quieres ver? ";
			std::cin>>item_a_mostrar;
			std::cout<<" Mostrado elemento: "<< lista.get_active_r(item_a_mostrar);
			break;
			
		case 5:
			for(int i=0; i<lista.size(); i++){
				if(lista.active(i)){
					lista.commute(i);
				}
			}
			std::cout<<lista.status()<<std::endl;
			break;

		default:
			if(opcion_elegida != condicion_de_salida){
				std::cout<<" opción: "<< opcion_elegida << " No disponible. "<<std::endl;
			}
			break;
	}		
	std::cout<<resolucion<<"\n ..."<<std::endl;

	// FIXMI: debo controlar el error cuando se pulsa una tecla no numérica
	std::cout<<strerror(errno)<<std::endl;

}




#ifndef _CABECERAS_DE_MENU_H_
#define _CABECERAS_DE_MENU_H_
/*
	Clase especializada para ser la interfaz de listado.h
*/

#include <string>
#include <vector>
#include "listado.h"
#include "informa.h"

class Menu{
	// Atributos
	private:
	int opcion_elegida;
	int condicion_de_salida;
	std::vector< std::string> opciones;
	Listado lista;
	
	void init();
	void realizar_opcion();
		
	// Métodos
	public:
	
	Menu();
	Menu( Listado& l );
	void set(Listado &l);
	Listado show();
	int get_option();
	bool exit();
	void reset_option();
	
};

#endif


#include <iostream>
#include "listado.h"
#include <vector>
#include <string>

int main(int argc, char* argv[]){
	std::string version = " Versión 1 ";
	
	Listado listado = Listado(argc, argv);
	
	//Si hubo_error en la construcción de listado, debemos salir de la ejecución
	bool salir = listado.get_informe().get_error();
	
	while(!salir){
		std::string menu = "\n Opciones disponibles: ";
		std::vector< std::string> opciones;
		opciones.push_back(" Mostrar todos los elementos.");
		opciones.push_back(" Mostrar los elementos activo.");
		opciones.push_back(" Mostrar los elementos inactivos.");
		opciones.push_back(" Activar/Desactivar elemento. ");
		opciones.push_back(" Salir");
	
	
		std::cout<< menu<< std::endl;
		for(int i=0; i<opciones.size(); i++){
			std::cout<< " ["<< i<< "] "<< opciones[i]<<std::endl;
		}
	
	
		std::cout<< " opción: ";
		int opcion;
		std::cin>>opcion;
		std::cout<<std::endl;

		std::string resolucion;
		switch (opcion){
			case 0:		// Mostrar todos los elementos
				resolucion = listado.status();
				break;
			
			case 1:		// Mostrar los elementos activos
				resolucion = listado.actives(true);
				break;
			
			case 2:		// Mostrar los elementos inactivos
				resolucion = listado.actives(false);
				break;
			
			case 3:		// Activar/Desactivar elemento
				std::cout<<listado.status()<<std::endl;
				std::cout<<" ¿Cual quiere conmutar? ";
				std::cin>>opcion;
				std::cout<<" opcion elegida: "<< opcion<< std::endl;
				listado.commute(opcion);
			break;
			
			case 4:
				salir = true;	
			break;

			default:
				std::cout<<" opción: "<< opcion<< " No disponible. "<<std::endl;
				break;
		}
		
		std::cout<<resolucion<< std::endl;
	
	}
	
	std::cout<<"\n Estado final:\n" <<listado.to_string()<<std::endl;
	
	

}
